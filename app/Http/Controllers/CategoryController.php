<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Auth;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display list of category
     */
    public function overview()
    {
        try{
            $categories = Category::where('user_id', Auth::id())->get();

            return view(
                'category.list',
                compact(
                    'categories'
                )
            );
        }
        catch(\Exception $e){
            return back()->with("error-message", "Something went wrong...".$e->getMessage());
        }
    }
    /**
     * show form for creating category
     */
    public function create()
    {
        return view('category.form');
    }
    /**
     * display category for update
     */
    public function view()
    {
        try{
            $category = Category::where('id', request('category_id'))
                ->where('user_id', Auth::id())
                ->firstOrFail();
                
            return view(
                'category.form',
                compact('category')
            );
        }
        catch(\Exception $e){
            return back()->with("error-message", "Something went wrong...".$e->getMessage());
        }
      
    }
    /**
     * creating a new category
     */
    public function store(CategoryRequest $request)
    {
        try{
            $validated = $request->validated();

            $category = Category::where('id', request('category_id'))
                ->where('user_id', Auth::id())
                ->first();
    
            if (!$category) {
                $category = new Category();
            }
    
            $validated['user_id'] = Auth::id();
            $category->fill($validated);
            $category->save();
    
            return redirect(route('category.list'))->with('status', 'Category Saved Successfully!');
        }
        catch(\Exception $e){
            return back()->with("error-message", "Something went wrong...".$e->getMessage());
        }
    }

}
