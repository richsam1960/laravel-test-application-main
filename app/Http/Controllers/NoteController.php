<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Models\Note;
use App\Models\Category;
use App\Models\NoteCategory;
use Auth;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function overview()
    {
        try{
            $notes = Note::with(['categories'])->where('user_id', Auth::id())->get();

            return view(
                'note.list',
                compact(
                    'notes'
                )
            );
        }
        catch(\Exception $e){
            return back()->with("error-message", "Something went wrong...".$e->getMessage());
        }
    }

    public function create()
    {
         try{
            $categories = Category::where('user_id', Auth::id())->get();
            return view(
                'note.form',
                compact('categories')
            );
        }
        catch(\Exception $e){
            return back()->with("error-message", "Something went wrong...".$e->getMessage());
        }
    }

    public function view()
    {
        try{
            $note_id = request('note_id');
            $note = Note::where('id', $note_id)
                ->where('user_id', Auth::id())
                ->firstOrFail();

            $categories = Category::where('user_id', Auth::id())->get();
            $existingNoteCategories = NoteCategory::where('note_id', $note_id)->pluck('category_id')->toArray();
                
            return view(
                'note.form',
                compact('note','categories','existingNoteCategories')
            );
        }
        catch(\Exception $e){
            return back()->with("error-message", "Something went wrong...".$e->getMessage());
        }
    }

    public function store(NoteRequest $request)
    {
       try{
         $validated = $request->validated();
         \DB::transaction(function () use($validated,$request){

                $note = Note::where('id', request('note_id'))
                    ->where('user_id', Auth::id())
                    ->first();

                if (!$note) {
                    $note = new Note();
                }

                $validated['user_id'] = Auth::id();
                $note->fill($validated);
                $note->save();

                //adding note category
                $selectedCategories = request('note_categories')?:array();
                $noteCategoryArray = array();
                $activityTime = $note->created_at;

                    foreach($selectedCategories as $category_id){
                    // $category = Category::find($category_id);
                    // $note->assignCategory($category);
                        $noteCategoryArray[] = [
                            "note_id" => $note->id,
                            "category_id" => $category_id,
                            "created_at" => $activityTime,
                            "updated_at" => $activityTime
                        ];
                    }
                    //create new note-category
                    NoteCategory::insertOrIgnore($noteCategoryArray);
                    //delete categories not selected
                    NoteCategory::where('note_id',$note->id)->whereNotIn('category_id',$selectedCategories)->delete();
            });

            return redirect(route('note.list'))->with('status', 'Note Saved');
        }
        catch(\Exception $e){
            return back()->with("error-message", "Something went wrong...".$e->getMessage());
        }
    }


}
