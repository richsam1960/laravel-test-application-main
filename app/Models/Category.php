<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

     //return the notes
     public function notes()
     {
         return $this->belongsToMany(Note::class, 'note_categories');
     }
}
