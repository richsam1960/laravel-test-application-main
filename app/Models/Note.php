<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    //return the categories
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'note_categories');
    }
    
    //assign a category to a note
    public function assignCategory($category)
    {
      //  if(!$this->hasCategory($category)){
            if (is_string($category)) {
                return $this->categories()->save(
                    Category::whereName($category)->firstOrFail()
                );
            } 
            return $this->categories()->save($category);
       // }
    }

    //has category
    public function hasCategory($category)
    {
        if (is_string($category)) {
            return !! $this->categories()->whereName($category)->first();
        }
        return !! $this->categories->find($category);
    } 

}
