@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-8">

            <div class="card">

                <div class="card-header">{{ __(isset($category) ? 'Edit Category' : 'Create Category') }} <a href="{{ route('category.list') }}" class="float-right btn btn-primary btn-sm">{{ __('Category List') }}</a> </div>

                <div class="card-body">
                    @if (session('error-message'))
                        <div class="alert alert-danger" role="alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('error-message') }}
                        </div>
                    @endif

                    <form action="{{ isset($category) ? route('category.store', $category->id) : route('category.store') }}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="name">{{ __('Category Name') }}*</label>
                            <input type="text" name="name" id="name" placeholder="Category Name" class="form-control @error('name') is-invalid @enderror" value="{{ isset($category) ? $category->name : old('name') }}">

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection
