@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Categories') }}<a href="{{ route('category.create') }}" class="float-right btn btn-primary btn-sm">{{ __('Create Category') }}</a></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('error-message'))
                        <div class="alert alert-danger" role="alert">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('error-message') }}
                        </div>
                    @endif

                    <ul class="list-group list-group-flush">
                        @forelse ($categories as $category)
                            <li class="list-group-item"><a href="{{ route('category.view', $category->id) }}">{{ $category->name }}</a></li>
                        @empty
                        <em>No category available, click here to <a href="{{ route('category.create') }}">{{ __('Create Category') }}</a></em>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
