@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-8">

            <div class="card">

                <div class="card-header">{{ __(isset($note) ? 'Edit Note' : 'Create Note') }}<a href="{{ route('note.list') }}" class="float-right btn btn-primary btn-sm">{{ __('Note List') }}</a></div>

                <div class="card-body">
                    @if (session('error-message'))
                        <div class="alert alert-danger" role="alert">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('error-message') }}
                        </div>
                    @endif

                    <form action="{{ isset($note) ? route('note.store', $note->id) : route('note.store') }}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="title">{{ __('Title') }}*</label>
                            <input type="text" name="title" id="title" placeholder="Title" class="form-control @error('title') is-invalid @enderror" value="{{ isset($note) ? $note->title : '' }}">

                            @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label for="content">{{ __('Content') }}</label>
                            <textarea name="content" id="content" rows="10" class="form-control @error('content') is-invalid @enderror" id="content">{{ isset($note) ? $note->content : '' }}</textarea>
                        </div>
                    
                    <div class="form-group">
                    <label for="content">{{ __('Categories') }} <small>{{ __('(select one or more category if applicable )') }}</small></label>
                            <div class="row">
                                    @forelse($categories as $category)
                                             <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="note_categories[]" id="note_category{{ $category->id }}" value="{{ $category->id }}" {{ isset($note) ? (in_array($category->id, $existingNoteCategories)?'checked':'') : '' }}>

                                                    <label class="form-check-label" for="note_category{{ $category->id }}">
                                                        {{ __($category->name) }}
                                                    </label>
                                                </div>
                                            </div>
                                            @empty
                                            <em>No category available, click here to <a href="{{ route('category.create') }}">{{ __('Create Category') }}</a></em>
                                    @endforelse
                            </div>
                   </div>

                        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection
