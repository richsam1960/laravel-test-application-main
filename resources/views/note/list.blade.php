@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Notes') }}<a href="{{ route('note.create') }}" class="float-right btn btn-primary btn-sm">{{ __('Create Note') }}</a></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              {{ session('status') }}
                        </div>
                    @endif

                    <ul class="list-group list-group-flush">
                        @foreach ($notes as $note)
                            <li class="list-group-item">
                                <span class="float-left">
                                   <a href="{{ route('note.view', $note->id) }}">{{ $note->title }}</a>
                                </span>
                               <span class="float-right">
                                @foreach($note->categories as $category)
                                  <small>** {{$category->name}}</small>
                                 @endforeach
                                 </span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
